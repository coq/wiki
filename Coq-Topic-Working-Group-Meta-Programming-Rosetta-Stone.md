The goal of this working group is to come up with an example for a meta programming / tactic language / plugin implemented in the various common meta programming languages, namely:

- OCaml (Enrico)
- Ltac (M. Soegtrop) (L. Dubois de Prisque)
- Mtac2 (Janno)
- Ltac2 (M. Soegtrop)
- MetaCoq (L. Dubois de Prisque, K. Maillard)
- Elpi (Enrico)

Details (meeting schedule) are TBD.

See also [Zulip chat](https://coq.zulipchat.com/#narrow/stream/237655-Miscellaneous/topic/Plugin.20development.20with.20Coq-Elpi.2C.20Template.2FMetaCoq.2C.20Mtac2.2E.2E.2E)