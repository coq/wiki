Maxime Dénès and Enrico Tassi are the release managers for 8.13

The following issue can be used to track the release process: https://github.com/coq/coq/issues/12334

## Schedule

- Branching date / feature freeze: November 16th
- 8.13+beta1 release: December 7th
- 8.13.0 release: January 7th