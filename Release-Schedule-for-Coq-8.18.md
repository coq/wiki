RM for 8.18: Enrico Tassi and Maxime Dénès

Release process issue: https://github.com/coq/coq/issues/17683

## Tentative schedule

- Branch creation: 2023-06-28
- 8.18+rc1: 2023-08-03
- 8.18.0: 2023-09-07
