What can we except(sic?) to have to do if we change the name:

- Changing the tool names (coqc, coqide, ...)
- Marketing: having a recognizable name / abbreviation is important (e.g. LPC?)
  e.g think about google searches

- Changes the imagery around Coq, logos, website, derived works as well, refman
- Changes in the ecosystem:
  - some projects whose name contains "coq" may require a renaming as well, eg. coq-elpi, js-coq, metacoq
  - packages: opam, snap, nix, debian, ... ? ...
  - coq-platform coq-community
- References in (future) papers would also change.
- In .v files `From Coq ...` (alias?)
- syntax highlighting (minted, github, editors), eg "```coq"
- Zulip chat
- Discourse forum
- XYZoverflow tags
- ...