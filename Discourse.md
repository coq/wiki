Coq has a Discourse forum located at https://coq.discourse.group.

This is an alternative to mailing lists such as Coq-Club that has several advantages:
- easier to browse past discussions
- easier for newcomers to participate: you don't have to subscribe to a mailing list to ask a question
- formatting, reactions, tags, categories...
- good e-mail support for those who prefer to continue using their e-mail client

This page will gather some tips and tricks about Discourse.

## Troubleshoot

- Logging in with GitHub is supported. But note that you may have to disable Adblock Plus on the Discourse site to use this feature.
- In case you need to merge several accounts (e.g. staged accounts created by sending emails), please contact our admin (see https://coq.discourse.group/about).

## E-mail support

### Receiving new messages by e-mail

You can activate mailing list mode globally in [your preferences](https://coq.discourse.group/my/preferences/emails). You can then mute some specific categories and unsubscribe from threads you are not interested in.

If you only want to receive e-mails for new messages in a selection of categories, and receive e-mail notifications for other categories only when you are mentioned or replied to, then you can "watch" these categories. Your e-mail preferences can be tweaked to receive e-mails even when you are active on the website.

### Sending new messages by e-mail

You can post a new message on an existing topic by replying to the e-mail notification.

To create a new topic by e-mail, write to one of these addresses (depending on the category your post belongs):
- coq+using-coq@discoursemail.com
- coq+plugin-development@discoursemail.com
- coq+coq-development@discoursemail.com
- coq+announcements@discoursemail.com
- coq+miscellaneous@discoursemail.com

And for the language-specific categories:
- coq+zh@discoursemail.com
- coq+es@discoursemail.com
- coq+fr@discoursemail.com
- coq+de@discoursemail.com
- coq+ru@discoursemail.com

*Warning*: formatting will be applied following the standard Markdown conventions. As a consequence, indentation will be lost unless you put more than four spaces (in which case your text will be interpreted as code and printed verbatim). Also, numerous dashes `--------` have sometimes been interpreted as marking the end of the message and led to the rest being cut out.

## Posting in other languages than English

We welcome new posts in other languages than English. For a selection of non-English languages for which we have moderators (see https://coq.discourse.group/about), we have created specific categories (Chinese, Spanish, French, German, Russian), but even if your language is not among those, you can use the main categories and open your post there. If we get enough people involved, we will create a category.