Emilio J. Gallego Arias and Théo Zimmerman are the release managers for 8.12

The following issue can be used to track the release process: https://github.com/coq/coq/issues/11157

## Tentative schedule

- Branching date / feature freeze: May 15th
- 8.12+beta1 release: first week of June
- 8.12.0 release: July (hopefully by the date of the Coq workshop)

## High-level overview

- Continuation of the API refactoring in order to improve integration with 3rd party plugins and tooling.
- Improving and stabilizing some experimental features such as grammar entries, vos support, etc...

## Compatibility changes