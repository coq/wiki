- October 21th 2020, 4pm-5pm Paris Time
- https://rdv2.rendez-vous.renater.fr/coq-call

## Topics

- Developer helpers to checkout for 3rd party developments c.f. https://github.com/coq/coq/pull/12674