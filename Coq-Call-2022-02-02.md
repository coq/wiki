- February 02, 2022, 4pm-5pm Paris Time
- https://rdv2.rendez-vous.renater.fr/coq-call

## Topics
- Discuss the Docker Hub rate limit issue and potential solutions (item proposed by Erik and Théo in [this Zulip topic](https://coq.zulipchat.com/#narrow/stream/237656-Coq-devs.20.26.20plugin.20devs/topic/Coq.20Docker.20rate.20limit/near/267707252) opened by Karl)
- GSOC 2022 + Outreachy (Emilio)
- Update on the Hackathon (Ali Emilio)
- Zulip permissions (Emilio)
- https://github.com/coq/coq/pull/15575 (PMP and Gaëtan)

## Notes

- GSOC 2022 + Outreachy (Emilio)
  Emilio will try to see if someone in the larger community is interested.

- Update on the Hackaton
  Program ready by next week.
  Should have a general "roadmap" sessions. Topics like tactics and a "new" stdlib would give good brainstorming sessions too.

- https://github.com/coq/coq/pull/15575

- On the Docker item: even if the issue only showed up twice, it is likely to happen more often, so to address it properly:
  1. we should check the availability of a Docker registry proxy (caching) CI/CD feature, in both GitLab CI and GitHub Actions;
  2. Mathieu may submit an open-source extended-support application, after some discussion on Zulip in the upcoming days;
  3. Erik may refactor the Docker-Coq infrastructure to sequentially push to several Docker registries (extending [`docker-keeper`](https://gitlab.com/erikmd/docker-keeper/) then changing [this line](https://github.com/coq-community/docker-coq/blob/1736d1577d3d015bd62f9f7801d99353e95db046/images.yml#L4))
  4. As a follow-up of item (iii), some documentation & automation may need some changes in [coq-community/templates](https://github.com/coq-community/templates).