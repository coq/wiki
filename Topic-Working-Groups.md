### Coq Topic Area Working Groups

This page is the entry point for the Coq Topic Area Working Groups.

The idea of these groups is to coordinate roadmaps and initiatives
around a concrete sub-area related to Coq development.

#### Format and participation

The main activity of the groups will be a periodic meeting to discuss and coordinate initiatives in the particular are of interest.

Each group has a coordinator which is responsible of scheduling and agenda; to show interest in the WG, please add your name to the participants lists.

#### Groups:

- [Topic Working Group: User Interfaces](Coq-Topic-Working-Group-User-Interfaces)
- [Topic Working Group: Document Data and Machine Learning Platform](Coq-Topic-Working-Group-Document-Data-and-Machine-Learning-Platform)
- [Topic Working Group: OCaml Multicore](Coq-Topic-Working-Group-Multicore)
- [Topic Working Group: Meta Programming Rosetta Stone](Coq-Topic-Working-Group-Meta-Programming-Rosetta-Stone)
