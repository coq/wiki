The First Rocq'n'code meeting (previously known as Coq Users and Developers Workshop) will take place at Inria Paris from Monday June 23rd (or 24th) to the 27th. 

Rocq'n'code is a meeting place for developers of the Rocq Prover and of its extensions: libraries, plugins, verification frameworks, compilers, decision procedures... The program will be a mix of scientific/technical talks and small group design/programming sessions.

Rooms
-----

- Monday/Tuesday: Emmy Noether
- Wednesday/Thursday: Gilles Kahn + Sally Floyd
- Friday: Anita Borg

Talk proposals
--------------

Add talk proposals here, they will be scheduled during the week (30min or 1h slots)

- A new, correct and complete algorithm for cumulativity checking with algebraic universes (Matthieu Sozeau) (30min).
- A new core library based on universe & sort polymorphism (Josselin Poiret, Matthieu Sozeau, Nicolas Tabareau, ...) (1h)
- 'Erased': a new sort for proof-relevant but computationally-irrelevant data (Johann Rosain, Matthieu Sozeau, Théo Winterhalter) (30min).
- ...

Project proposals
-----------------

Add project proposals here, they will be scheduled during the week and 

- ...
