# Past WG

[Dune Working Group Summer 2022](DuneWG-2022)

# Topics

- coq lang 1.0
  + critical bug: https://github.com/ocaml/dune/issues/7908
  + coq-package file
- test suite Coq https://github.com/coq/coq/pull/16583
- Recording of perf data https://github.com/ocaml/dune/issues/7460
- coq-lsp dune support
