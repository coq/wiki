RM for 8.17: Théo Zimmermann (co-RM: Gaëtan Gilbert)

Release process issue: https://github.com/coq/coq/issues/16820

## Tentative schedule

- Branch created on: 2022-11-21
- 8.17+rc1: 2022-12-16
- 8.17.0: ~2023-01-16~ 2023-03-23
- 8.17.1: 2023-06-27
- 8.17.2: TBD