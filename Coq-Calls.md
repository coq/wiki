Coq (quasi) weekly Developer Calls
----------------------------------

By convention, the page for the next Coq call is created by the first person wishing to add a topic.
If the page for the next Coq call does not exist yet, do not hesitate
to create it by duplicating a previous one and adding your topics there.
BEWARE: The calls are now on Tuesdays at 4pm Paris time rather than Wednesdays.

For last-minute details about who will be available for a call or whether a call will be postponed, see
[Zulip](https://coq.zulipchat.com/#narrow/stream/237656-Coq-devs.20.26.20plugin.20devs/topic/Coq.20Call).

### Guidelines

The general [rules for Coq meetings](Coq-Meetings-Organization) apply.

- Add any topic you want the team to discuss _along with_ the name of the discussion leader *and* a time estimate for easier scheduling.
- Topics should be set well in advance, by Monday 4pm at the latest.
- One cannot take more than a 30-minute slot. If a subject requires more time, setup a specific conference call on it.

### Joining the call

Lately, we have been using https://rdv2.rendez-vous.renater.fr/coq-call. Check on the top of the page for the current call for most up-to-date information.


List of Calls:
--------------

Format for the page is `YYYY-MM-DD.md`:

- [Coq-Call-2025-03-04](Coq-Call-2025-03-04)

Past calls

- [Coq-Call-2025-02-25](Coq-Call-2025-02-25)
- No Coq Call on 2025-02-18 (Holidays)
- No Coq Call on 2025-02-11 (Holidays)
- No Coq Call on 2025-02-04 (no topics)
- [Coq-Call-2025-01-28](Coq-Call-2025-01-28)
- [Coq-Call-2025-01-21](Coq-Call-2025-01-21)
- [Coq-Call-2025-01-14](Coq-Call-2025-01-14)
- [Coq-Call-2025-01-07](Coq-Call-2025-01-07)
- No Coq Call on 2024-12-31 (holiday)
- No Coq Call on 2024-12-24 (holiday)
- [Coq-Call-2024-12-17](Coq-Call-2024-12-17)
- [Coq-Call-2024-12-10](Coq-Call-2024-12-10)
- [Coq-Call-2024-12-03](Coq-Call-2024-12-03)
- No Coq Call on 2024-11-26 (no topics)
- [Coq-Call-2024-11-19](Coq-Call-2024-11-19)
- [Coq-Call-2024-11-12](Coq-Call-2024-11-12)
- [Coq-Call-2024-11-05](Coq-Call-2024-11-05)
- [Coq-Call-2024-10-29](Coq-Call-2024-10-29)
- [Coq-Call-2024-10-22](Coq-Call-2024-10-22)
- No Coq Call on 2024-10-15 (Inria prospective seminar)
- [Coq-Call-2024-10-08](Coq-Call-2024-10-08)
- [Coq-Call-2024-10-01](Coq-Call-2024-10-01)
- No Coq Call on 2024-09-24 (Galinette yearly retreat)
- [Coq-Call-2024-09-17](Coq-Call-2024-09-17)
- [Coq-Call-2024-09-10](Coq-Call-2024-09-10)
- No Coq Call on 2024-09-03
- [Coq-Call-2024-08-27](Coq-Call-2024-08-27)
- No Coq Calls from 2024-07-09 to 2024-08-20 (Summer break)
- No Coq Call on 2024-07-02 (CUDW)
- No Coq Call on 2024-06-25
- No Coq Call on 2024-06-18 
- No Coq Call on 2024-06-11 (TYPES)
- [Coq-Call-2024-06-04](Coq-Call-2024-06-04)
- [Coq-Call-2024-05-28](Coq-Call-2024-05-28)
- [Coq-Call-2024-05-21](Coq-Call-2024-05-21)
- No Coq Call on 2024-05-14
- [Coq-Call-2024-05-07](Coq-Call-2024-05-07)
- [Coq-Call-2024-04-30](Coq-Call-2024-04-30)
- [Coq-Call-2024-04-23](Coq-Call-2024-04-23)
- No Coq Call on 2024-04-16
- [Coq-Call-2024-04-09](Coq-Call-2024-04-09)
- [Coq-Call-2024-04-02](Coq-Call-2024-04-02)
- [Coq-Call-2024-03-26](Coq-Call-2024-03-26)
- [Coq-Call-2024-03-19](Coq-Call-2024-03-19)
- No Coq Call on 2024-03-12 ([CoREACT](https://coreact.wiki/) meeting)
- [Coq-Call-2024-03-05](Coq-Call-2024-03-05)
- [Coq-Call-2024-02-19](Coq-Call-2024-02-19)
- [Coq-Call-2024-02-12](Coq-Call-2024-02-12)
- [Coq-Call-2024-02-05](Coq-Call-2024-02-05)
- [Coq-Call-2024-01-29](Coq-Call-2024-01-29)
- [Coq-Call-2024-01-22](Coq-Call-2024-01-22)
- [Coq-Call-2024-01-15](Coq-Call-2024-01-15)
- No Coq Call on 2024-01-08
- [Coq-Call-2023-12-11](Coq-Call-2023-12-11)
- [Coq-Call-2023-12-04](Coq-Call-2023-12-04)
- [Coq-Call-2023-11-21](Coq-Call-2023-11-21)
- [Coq-Call-2023-11-14](Coq-Call-2023-11-14)
- [Coq-Call-2023-11-07](Coq-Call-2023-11-07)
- [Coq-Call-2023-10-31](Coq-Call-2023-10-31)
- [Coq-Call-2023-10-24](Coq-Call-2023-10-24)
- No Coq Call on 2023-10-17 (workshop)
- [Coq-Call-2023-10-10](Coq-Call-2023-10-10)
- [Coq-Call-2023-10-03](Coq-Call-2023-10-03)
- [Coq-Call-2023-09-26](Coq-Call-2023-09-26)
- [Coq-Call-2023-09-19](Coq-Call-2023-09-19)
- [Coq-Call-2023-09-12](Coq-Call-2023-09-12)
- [Coq-Call-2023-09-05](Coq-Call-2023-09-05)
- ~~[Coq-Call-2023-08-08](Coq-Call-2023-08-08)~~ (did not happen due to lack of participants)
- [Coq-Call-2023-07-18](Coq-Call-2023-07-18)
- [Coq-Call-2023-07-11](Coq-Call-2023-07-11)
- No Coq Call on 2023-07-04 (no topics)
- No Coq Call on 2023-06-27 (CUDW 2023)
- No Coq Call on 2023-06-20 (no topics)
- No Coq Call on 2023-06-13 (no topics)
- [Coq-Call-2023-06-06](Coq-Call-2023-06-06)
- No Coq Call on 2023-05-30 (postponed due to lack of available participants)
- [Coq-Call-2023-05-23](Coq-Call-2023-05-23)
- [Coq-Call-2023-05-16](Coq-Call-2023-05-16)
- [Coq-Call-2023-05-09](Coq-Call-2023-05-09)
- No Coq Call on 2023-05-02 (no topics)
- [Coq-Call-2023-04-25](Coq-Call-2023-04-25)
- No Coq Call on 2023-04-18 (no topics)
- No Coq Call on 2023-04-11 (no topics)
- [Coq-Call-2023-04-04](Coq-Call-2023-04-04)
- [Coq-Call-2023-03-28](Coq-Call-2023-03-28)
- No Coq Call on 2023-03-21 (no topics)
- No Coq Call on 2023-03-14 (no topics)
- [Coq-Call-2023-03-08](Coq-Call-2023-03-08)
- [Coq-Call-2023-03-01](Coq-Call-2023-03-01)
- No Coq Call on 2023-02-22 (holidays & ITP deadline)
- No Coq Call on 2023-02-15 (holidays)
- [Coq-Call-2023-02-08](Coq-Call-2023-02-08)
- [Coq-Call-2023-01-25](Coq-Call-2023-01-25)
- [Coq-Call-2023-01-18](Coq-Call-2023-01-18)
- [Coq-Call-2023-01-11](Coq-Call-2023-01-11)
- [Coq-Call-2023-01-04](Coq-Call-2023-01-04)
- No Coq Call on 2022-12-14
- No Coq Call on 2022-12-07 (math-comp workshop)
- [Coq-Call-2022-11-30](Coq-Call-2022-11-30)
- No Coq Call on 2022-11-23
- [Coq-Call-2022-11-16](Coq-Call-2022-11-16)
- No Coq Call on 2022-11-09
- [Coq-Call-2022-11-02](Coq-Call-2022-11-02)
- No Coq Call on 2022-10-26
- [Coq-Call-2022-10-19](Coq-Call-2022-10-19)
- [Coq-Call-2022-10-12](Coq-Call-2022-10-12)
- [Coq-Call-2022-10-05](Coq-Call-2022-10-05)
- [Coq-Call-2022-09-28](Coq-Call-2022-09-28)
- [Coq-Call-2022-09-21](Coq-Call-2022-09-21)
- No Coq Call on 2022-09-14
- No Coq Call on 2022-09-07
- [Coq-Call-2022-08-31](Coq-Call-2022-08-31)
- No Coq Call on 2022-08-24
- [Coq-Call-2022-08-17](Coq-Call-2022-08-17)
- No Coq Call from 2022-07-06 to 2022-08-15 (Holidays)
- [Coq-Call-2022-06-29](Coq-Call-2022-06-29)
- No Coq Call on 2022-06-22 (during TYPES)
- No Coq Call on 2022-06-15 (WG)
- No Coq Call on 2022-06-08
- [Coq-Call-2022-06-01](Coq-Call-2022-06-01)
- [Coq-Call-2022-05-25](Coq-Call-2022-05-25)
- [Coq-Call-2022-05-18](Coq-Call-2022-05-18)
- [Coq-Call-2022-05-11](Coq-Call-2022-05-11)
- [Coq-Call-2022-05-04](Coq-Call-2022-05-04)
- [Coq-Call-2022-04-27](Coq-Call-2022-04-27)
- No Coq Call on 2022-04-20, Easter break continued
- No Coq Call on 2022-04-13, Postponed due to easter holidays
- [Coq-Call-2022-04-06](Coq-Call-2022-04-06)
- No Coq Call on 2022-03-30
- No Coq Call on 2022-03-23
- [Coq-Call-2022-03-16](Coq-Call-2022-03-16)
- [Coq-Call-2022-03-09](Coq-Call-2022-03-09)
- No Coq Call on 2022-03-02
- [Coq-Call-2022-02-23](Coq-Call-2022-02-23)
- No Coq Call on 2022-02-16 (WG)
- [Coq-Call-2022-02-09](Coq-Call-2022-02-09)
- [Coq-Call-2022-02-02](Coq-Call-2022-02-02)
- [Coq-Call-2022-01-26](Coq-Call-2022-01-26)
- No Coq Call on 2022-01-19
- [Coq-Call-2022-01-12](Coq-Call-2022-01-12)
- [Coq-Call-2022-01-05](Coq-Call-2022-01-05)
- [Coq-Call-2021-12-15](Coq-Call-2021-12-15)
- [Coq-Call-2021-12-08](Coq-Call-2021-12-08)
- [Coq-Call-2021-12-01](Coq-Call-2021-12-01)
- No Coq Call on 2021-11-24 (no topics raised)
- [Coq-Call-2021-11-17](Coq-Call-2021-11-17)
- [Coq-Call-2021-11-10](Coq-Call-2021-11-10)
- [Coq-Call-2021-11-03](Coq-Call-2021-11-03)
- [Coq-Call-2021-10-27](Coq-Call-2021-10-27)
- [Coq-Call-2021-10-20](Coq-Call-2021-10-20)
- [Coq-Call-2021-10-13](Coq-Call-2021-10-13)
- No Coq Call on 2021-10-06 (postponed because of Galinette seminar)
- [Coq-Call-2021-09-29](Coq-Call-2021-09-29)
- [Coq-Call-2021-09-22](Coq-Call-2021-09-22)
- [Coq-Call-2021-09-15](Coq-Call-2021-09-15)
- [Coq-Call-2021-09-08](Coq-Call-2021-09-08)
- [Coq-Call-2021-09-01](Coq-Call-2021-09-01)
- [Coq-Call-2021-07-13](Coq-Call-2021-07-13) (NB: the date is not a typo, this is on Tuesday as the 14th is a French holiday)
- [Coq-Call-2021-07-07](Coq-Call-2021-07-07)
- [Coq-Call-2021-06-30](Coq-Call-2021-06-30)
- [Coq-Call-2021-06-23](Coq-Call-2021-06-23)
- [Coq-Call-2021-06-16](Coq-Call-2021-06-16)
- No Coq Call on 2021-06-09
- No Coq Call on 2021-06-02
- [Coq-Call-2021-05-26](Coq-Call-2021-05-26)
- [Coq-Call-2021-05-19](Coq-Call-2021-05-19)
- [Coq-Call-2021-05-12](Coq-Call-2021-05-12)
- [Coq-Call-2021-05-05](Coq-Call-2021-05-05)
- [Coq-Call-2021-04-28](Coq-Call-2021-04-28)
- [Coq-Call-2021-04-21](Coq-Call-2021-04-21)
- [Coq-Call-2021-04-07](Coq-Call-2021-04-07)
- [Coq-Call-2021-03-31](Coq-Call-2021-03-31)
- [Coq-Call-2021-03-24](Coq-Call-2021-03-24)
- [Coq-Call-2021-03-17](Coq-Call-2021-03-17)
- [Coq-Call-2021-03-10](Coq-Call-2021-03-10)
- [Coq-Call-2021-03-03](Coq-Call-2021-03-03)
- [Coq-Call-2021-02-24](Coq-Call-2021-02-24)
- [Coq-Call-2021-02-17](Coq-Call-2021-02-17)
- [Coq-Call-2021-02-10](Coq-Call-2021-02-10)
- [Coq-Call-2021-02-03](Coq-Call-2021-02-03)
- [Coq-Call-2021-01-27](Coq-Call-2021-01-27)
- No Coq Call on 2021-01-20 (week of CoqPL and POPL)
- [Coq-Call-2021-01-13](Coq-Call-2021-01-13)
- No Coq Call on 2021-01-06
- [Coq-Call-2020-12-16](Coq-Call-2020-12-16)
- [Coq-Call-2020-12-09](Coq-Call-2020-12-09)
- No Coq Call on 2020-12-02 (week of the [Coq-Users-Developers-Workshop-2020](../Coq-Users-and-Developers-Workshop-2020))
- [Coq-Call-2020-11-25](Coq-Call-2020-11-25)
- [Coq-Call-2020-11-18](Coq-Call-2020-11-18)
- [Coq-Call-2020-11-13](Coq-Call-2020-11-13)
- [Coq-Call-2020-11-04](Coq-Call-2020-11-04)
- [Coq-Call-2020-10-21](Coq-Call-2020-10-21)
- [Coq-Call-2020-10-07](Coq-Call-2020-10-07)
- [Coq-Call-2020-09-23](Coq-Call-2020-09-23)
- [Coq-Call-2020-09-16](Coq-Call-2020-09-16)
- [Coq-Call-2020-09-09](Coq-Call-2020-09-09)
- [Coq-Call-2020-09-02](Coq-Call-2020-09-02)
- [Coq-Call-2020-07-22](Coq-Call-2020-07-22)
- [Coq-Call-2020-07-15](Coq-Call-2020-07-15)
- Coq-Call-2020-07-08: postponed to next week
- Coq-Call-2020-07-01: postponed to next week
- [Coq-Call-2020-06-24](Coq-Call-2020-06-24)
- [Coq-Call-2020-06-17](Coq-Call-2020-06-17)
- Coq-Call-2020-06-10: no topics to discuss
- [Coq-Call-2020-06-03](Coq-Call-2020-06-03)
- Coq-Call-2020-05-27: no topics to discuss
- [Coq-Call-2020-05-20](Coq-Call-2020-05-20)
- [Coq-Call-2020-05-13](Coq-Call-2020-05-13)
- Coq-Call-2020-05-04 no topics to discuss. [CoqWG 2020-05-04](CoqWG-2020-05-04) happened that week.
- [Coq-Call-2020-04-22](Coq-Call-2020-04-22)
- Coq-Call-2020-04-15 no topics to discuss.
- [Coq-Call-2020-04-08](Coq-Call-2020-04-08)
- [Coq-Call-2020-04-01](Coq-Call-2020-04-01)
- [Coq-Call-2020-03-25](Coq-Call-2020-03-25)
- [Coq-Call-2020-03-18](Coq-Call-2020-03-18)
- [Coq-Call-2020-03-11](Coq-Call-2020-03-11)
- [Coq-Call-2020-03-04](Coq-Call-2020-03-04)
- [Coq-Call-2020-02-26](Coq-Call-2020-02-26)
- [Coq-Call-2020-02-19](Coq-Call-2020-02-19)
- [Coq-Call-2020-02-12](Coq-Call-2020-02-12)
- [Coq-Call-2020-02-05](Coq-Call-2020-02-05)
- [Coq-Call-2020-01-29](Coq-Call-2020-01-29)
- [Coq-Call-2020-01-22](Coq-Call-2020-01-22)
- [Coq-Call-2020-01-15](Coq-Call-2020-01-15)
- [Coq-Call-2019-12-11](Coq-Call-2019-12-11)
- [Coq-Call-2019-12-04](Coq-Call-2019-12-04)
- [Coq-Call-2019-11-27](Coq-Call-2019-11-27)
- [Coq-Call-2019-11-20](Coq-Call-2019-11-20)
- [Coq-Call-2019-11-13](Coq-Call-2019-11-13)
- [Coq-Call-2019-11-06](Coq-Call-2019-11-06)
- [Coq-Call-2019-10-23](Coq-Call-2019-10-23)
- [Coq-Call-2019-10-16](Coq-Call-2019-10-16)
- [Coq-Call-2019-09-25](Coq-Call-2019-09-25)
- [Coq-Call-2019-09-18](Coq-Call-2019-09-18)
