This page describes briefly how Coq users can verify that a Coq release tarball was signed by Coq a release manager. We will use [Coq 8.11.2](https://github.com/coq/coq/releases/tag/V8.11.2) and its tarball https://github.com/coq/coq/archive/V8.11.2.tar.gz as an example.

## Obtaining the checksum of the tarball

```shell
wget https://github.com/coq/coq/archive/V8.11.2.tar.gz
sha512sum V8.11.2.tar.gz
```
This yields a checksum of `f8ab307b8e39ffda5f6984e187c1f8de1cb6dec5c322726dbbe535ee611683cfeeb9cee3e11ad83f5e44e843fc51e7e2d50b4ea69ab42fde38aaf3d0cf2dea3c`

## Obtaining the release commit and signature

```shell
git clone https://github.com/coq/coq.git
cd coq
git show-ref -s V8.11.2
```
This yields a commit SHA of `1539a9a0d894ea9e24e3d0e163006d6477646ec6`.

## Verifying the signature of the commit

```shell
git verify-commit 1539a9a0d894ea9e24e3d0e163006d6477646ec6
```
This gives output along the following lines:
```
gpg: Signature made Thu 14 May 2020 03:29:39 PM CDT using RSA key ID D9BA3C7B
```
Here, one has to verify that the RSA key ID `D9BA3C7B` belongs to the release manager, which is beyond the scope of this discussion.

## Generating the tarball from the verified tag

```shell
git archive --format=tar.gz --prefix=coq-8.11.2/ -o /tmp/V8.11.2.tar.gz V8.11.2
sha512sum /tmp/V8.11.2.tar.gz
```
This gives the expected output `f8ab307b8e39ffda5f6984e187c1f8de1cb6dec5c322726dbbe535ee611683cfeeb9cee3e11ad83f5e44e843fc51e7e2d50b4ea69ab42fde38aaf3d0cf2dea3c`

## Conclusion

Since the checksums match, the tarball at https://github.com/coq/coq/archive/V8.11.2.tar.gz has been signed by the release manager (under usual cryptographic assumptions).