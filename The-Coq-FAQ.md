This FAQ is the sum of the questions that came to mind as we developed proofs in Coq. Since we are singularly short-minded, we wrote the answers we found on bits of papers to have them at hand whenever the situation occurs again. This is pretty much the result of that: a collection of tips one can refer to when proofs become intricate. Yes, this means we won't take the blame for the shortcomings of this FAQ. But if you want to contribute and send in your own question and answers, feel free to add your own contributions to this wiki.

- [Presentation](Presentation)
- [Documentation](Documentation)
- [Installation](Installation)
- [Getting started](Getting-Started)
- [The Logic of Coq](The-Logic-of-Coq)
  - [General](The-Logic-of-Coq#general)
  - [Axioms](The-Logic-of-Coq#axioms)
  - [Impredicativity](The-Logic-of-Coq#impredicativity)
- [Talkin' with the Rooster](Talkin'-with-the-Rooster)
  - [My goal is ..., how can I prove it?](Talkin'-with-the-Rooster#my-goal-is--how-can-i-prove-it)
  - [Tactics Usage](Talkin'-with-the-Rooster#tactics-usage)
  - [Proof Management](Talkin'-with-the-Rooster#proof-management)
- [Inductive and Co-Inductive Types](Inductive-and-Co-Inductive-Types)
  - [General](Inductive-and-Co-Inductive-Types#general)
  - [Recursion](Inductive-and-Co-Inductive-Types#recursion)
  - [Co-Inductive Types](Inductive-and-Co-Inductive-Types#co-inductive-types)
- [Syntax and Notations](Syntax-and-Notations)
- [Ltac](Ltac)
- [Typeclasses and Canonical Structures](Typeclasses-and-Canonical-Structures)
- [Case Studies](Case-Studies)
- [Publishing Tools](Publishing-Tools)
- [CoqIde](CoqIde)
- [Extraction](Extraction)
- [Anti-patterns](Anti-patterns)
- [Setting up a development environment for working on Coq](DevelSetup)
- [Tactics Written in OCaml](Tactics-Written-in-OCaml)
- [Glossary](Glossary)
- [Troubleshooting](Troubleshooting)
- [More questions](#more-questions)

# More questions

## How to find unused lemmas in a large development?

Use the [dpdusage](https://github.com/Karmaki/coq-dpdgraph#dpdusage-find-unused-definitions-via-dpd-file) tool which comes with the [coq-dpdgraph](https://github.com/Karmaki/coq-dpdgraph) plugin.

## Questions to move to the section they belong to

- Should I put my type in [[Prop_or_Set]]?
- How does the [[MatchAsInReturn]] syntax work?
- Why can't I prove `(forall x, f x = g x) -> f = g`? See [[extensional_equality]].
- Isn't [[IntensionalEquality]] useless?
- How do I get [[DependentInversion]] to work in my case?
- Why not [[WTypeInsteadOfInductiveTypes]]?
- Do objects living in `Prop` ever need to be evaluated? See [[PropsGuardingIotaReduction]].
- When using `eapply`, how can I instantiate the question marks i.e. the [[ExistentialVariablesInEapply]]?
- [[CoqNewbieQuestions]]
