Pierre-Marie Pédrot & Matthieu Sozeau are the release managers (RM) for 8.11.

As to roughly follow the 6-month release cycle, here is the proposed schedule:

 - 2019-11-01: creation of the 8.11 branch (aka feature freeze), seven months after the creation of the 8.10 branch (see https://github.com/coq/coq/releases/tag/V8.11%2Balpha)
 - 2019-12-01: first β version
 - 2019-01-01: release of Coq version 8.11.0

Hopefully, the release will even be out before Christmas!

Some major changes are expected for that release: Ltac2, vos, VSCoq support, primitive floats (tentatively)...

Fixes:
- Import/Export inconsistency fixes
- Universes and the global env.

The following issue can be used to track the release process:
  https://github.com/coq/coq/issues/10843

