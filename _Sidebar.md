[Home](https://github.com/coq/coq/wiki)

## Installation of Coq
- [on Linux](Installation%20of%20Coq%20on%20Linux)
- [on Windows](Installation%20of%20Coq%20on%20Windows)
- [on macOS](Installation%20of%20Coq%20on%20Mac)

## Documentation pages in this wiki
- [The Coq FAQ](The-Coq-FAQ)
- [Axioms that you might consider adding to Coq](CoqAndAxioms)

## Events
- [Coq *weekly* Calls](Coq-Calls)
- [Coq ~~*monthly*~~ Working Groups](Coq-Working-Groups)
- [Coq *topic* Working Groups](Topic-Working-Groups)
- [Coq *yearly* Users and Developers Workshops](CoqImplementorsWorkshop)
- [Release Plan](Release-Plan)

<!-- newlines are nice for git diff -->
