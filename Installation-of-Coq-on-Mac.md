The recommended installation method is through the Coq platform. Other installation methods are also documented below.

## Coq platform

The Coq platform is a distribution of the Coq proof assistant together
with a selection of Coq libraries. It provides a set of scripts to
compile and install opam, Coq, external Coq libraries and Coq plugins on macOS,
Windows and many Linux distributions in a reliable way with consistent
results.

### Binary package

Binary installers for the Coq platform on macOS are not yet available. In the meantime, binary installers for just Coq + CoqIDE on macOS can be found at https://github.com/coq/coq/releases/latest.

### Installation from sources

Experienced users can rely on the Coq platform interactive scripts to install the Coq platform from sources using opam.
This will create an opam switch with Coq and a standard set of packages, which can then be modified, e.g., by installing additional packages.

See the macOS specific instructions for the Coq platform scripts here: https://github.com/coq/platform/blob/2021.02/README_macOS.md

Once the Coq platform is set up, you can install additional Coq packages by running `opam install coq-packagename`.

### User interfaces

You will need a user interface to run Coq. The Coq platform bundles CoqIDE, which is a Coq-specific editor. Alternatively, you can install editor-support packages for VsCode, Emacs or Vim. See https://coq.inria.fr/user-interfaces.html for details.

## Manual installation of Coq through opam

Complete instructions are available at https://coq.inria.fr/opam-using.html. They can be useful if you need to manage several versions of Coq and use specific versions of the OCaml compiler. Otherwise, we advise installing Coq through the Coq platform interactive scripts (see above).

## Installation from package repositories

[![latest packaged version(s)](https://repology.org/badge/latest-versions/coq.svg)](https://repology.org/metapackage/coq/versions)

![Homebrew package](https://repology.org/badge/version-for-repo/homebrew/coq.svg) \
![MacPorts package](https://repology.org/badge/version-for-repo/macports/coq.svg) \
![nixpkgs unstable package](https://repology.org/badge/version-for-repo/nix_unstable/coq.svg)
![nixpkgs stable package](https://repology.org/badge/version-for-repo/nix_stable/coq.svg)

You can install Coq with Homebrew by simply running

```
brew install coq
```

or using MacPorts by running

```
sudo port install coq
```

or using the [[Nix]] package manager running

```
nix-env -iA nixpkgs.coq
```

You can check that your installation was successful by running `coqc -v`.