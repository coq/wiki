As we approach the 10000th issue, Coq developers needs a new challenge. The principle is very simple, please place a bet (one per person) about the day we will reach the 10000th issue on Coq's Github page.

The winner will be the person placing their bet as close as possible to the actual date. Please use one line per person.

- 20190404 @ejgallego
- 20190418 @akaalharbi

Actual date: 20190425.