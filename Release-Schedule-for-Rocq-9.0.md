RM for 9.0: Pierre-Marie Pédrot

Release process issue: https://github.com/coq/coq/issues/19974

## Tentative schedule

- Branch creation: 2025-01-14
- 9.0+rc1: 2025-01-24
