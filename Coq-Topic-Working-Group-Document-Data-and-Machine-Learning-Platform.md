### Goals

The goal of this working group is to coordinate the improvement of Coq
aspects related to the processing of Coq with external tools, in
particular to ensure we provide a good API for such tools to interact
with Coq and to transform Coq documents.

### Meetings

It seems that a bi-monthly meeting could work, but still to be
discussed, we will soon add a poll.

- Kick-off meeting: TBA

### Zulip Stream for Discussion

- https://coq.zulipchat.com/#narrow/stream/252087-Machine-learning.20and.20automation

### Participants [in alphabetical order]

- Emilio J. Gallego Arias

### Topics and Related Projects

- Incremental Checking
- Document format / transformation
- Python bindings
