Guillaume Melquiond is the release manager for 8.14.

The following issue can be used to track the release process: https://github.com/coq/coq/issues/14893

## Schedule

- Branching date / feature freeze: May 31th, 2021
- 8.14+rc1 tag: ~June 14th, 2021~ after https://github.com/coq/coq/issues?q=is%3Aopen+is%3Aissue+milestone%3A8.14%2Brc1+label%3A%22priority%3A+blocker%22 are resolved
- 8.14.0 release: June 28th, 2021
