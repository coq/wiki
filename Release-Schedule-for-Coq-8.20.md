RM for 8.20: Pierre Roux (co-RM: Guillaume Melquiond)

Release process issue: https://github.com/coq/coq/issues/18882

## Tentative schedule

- Branch creation: 2024-06-17
- 8.20+rc1: 2024-06-26
- 8.20.0: 2024-09-04
- 8.20.1: 2025-01
- no 8.20.2
