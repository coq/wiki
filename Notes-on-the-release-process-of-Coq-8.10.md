# Notes on the release process of Coq 8.10

## Time-based release cycle

  - April 15th: branch `v8.10`
  - May 14th: version 8.10+β1
      - 28 PRs merged into `master` + 75 backported to `v8.10`
      - native integers
      - SProp
      - CoqIDE cannot be built on MS-Windows
      - Template polymorphism is known to be broken
  - Expect to release by the end of June
  
## Feature-based release cycle

  - June 20th: version 8.10+β2
      - 71 PRs merged into `master` + 50 backported to `v8.10`
      - CoqIDE fixed on MS-Windows
  - September 16th: version 8.10+β3
      - 95 PRs merged into `master` + 53 backported to `v8.10`
      - Template polymorphism issue partially addressed
  - Since then
      - 11 PRs merged into `master` + 2 backported to `v8.10`
      - A few open issues (`notypeclass refine`, `-topfile`…)

## Questions

  - Are we ready to release Coq 8.10.0?
  - Should the 8.11 release process start right after said release?
