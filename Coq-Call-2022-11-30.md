- November 30th 2022, 4pm Paris Time
- https://rdv2.rendez-vous.renater.fr/coq-call

## Topics

- Organization/time of the Coq Call (Matthieu, 10min)
- Voting process (Matthieu, 5min) 
- OCaml 5.0 is releasing soon, how can we make progress? (Ali, 15min)
  - https://github.com/coq/coq/issues/13940
  - https://github.com/coq/coq/projects/55#card-85963917

## Notes

- Coq Call organization
  * Suggestion to record calls in some private channel. Using webex?
  * TODO: Write the Coq Call guidelines on the Coq Calls page
- Voting process: ok from Hugo and Guillaume.
  Hugo makes the point that we should at least reach a consensus on the 
  consequences of a choice before we take a decision or even a vote.
- OCaml 5.0.
  - no-naked-pointers not an issue anymore if we disable native compilation
  - retargeting native-compilation to use malfunction might be the way to go
- Next Coq WG: 30th of january - 10th of february doodle