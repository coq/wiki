1. Install `coq-lsp` server from opam
2. Install `coq-lsp` client from the vscode marketplace.

Things now should work out of the box, open the folder where your `_CoqProject` file is located.

**Important:** be sure that client/server version are the same!

The "Settings" page of the `coq-lsp` extension contains all the options you can access.