(Part of [[the Coq FAQ]])

### How can I generate some LaTex from my development?

You can use `coqdoc`.

### How can I generate some HTML from my development?

You can use `coqdoc`.

### How can I generate a dependency graph from my development?

You can use the [coq-dpdgraph](https://github.com/Karmaki/coq-dpdgraph/).

### How can I cite some Coq in my LaTex document?

You can use `coq-tex`.

### How can I cite the Coq reference manual?

See instructions on the Zenodo record:

[![DOI][doi-badge]][doi-link]

[doi-badge]: https://zenodo.org/badge/DOI/10.5281/zenodo.1003420.svg
[doi-link]: https://doi.org/10.5281/zenodo.1003420

### Where can I publish my developments in Coq?

You can submit your developments to the [Coq Package Index](https://coq.inria.fr/packages).

An earlier related projet (unmaintained) was the HELM/MoWGLI repository at the University of Bologna (see http://mowgli.cs.unibo.it). For developments submitted in this database, it was possible to visualize them in natural language and execute various retrieving requests.
