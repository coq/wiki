This document describes the rules of participation and roles
involved in Coq meetings (calls, working groups and other meetings
organized by Coq team members). These were approved on [28th March 2023](Coq-Call-2023-03-28).

Obviously, our [code of conduct](https://github.com/coq/coq/blob/master/CODE_OF_CONDUCT.md) also applies during these meetings.

Motivation
----------

- Improve our organization and guidelines for teamwork and meetings in particular.
- Establish rules and roles for meetings.

Roles
-----

There are four different roles for meeting participants:

### Speakers

- Announce your topic well in advance (e.g., at least 1 day before for a Coq Call).
- Propose the duration of your talk which the chairman will use to create a schedule, which he/she will strictly enforce.
- Prepare your presentation in advance.  It is time well spent! It is time gained for your audience.  A brief write up can be very helpful.
- Do not go off-topic (off topics can be scheduled for a later meeting).

### Attendees

- Raise your hand to ask questions and let the chairman give you the floor.
- Do not lead the discussion off-topic.

### Chairman

- Decides a definitive schedule for the meeting in advance (e.g., before noon on a Coq Call day)
  staying within bounds and being fair to all speakers.
- The chairman must not be a speaker during that meeting.
- Keeps time during the meeting.
- Stops the discussion if it goes off topic.
  You can propose scheduling a new discussion at another time.
- Is the only one who can interrupt a speaker.

The chairman role rotates among attendees.

### Secretary

- Distinct role from the chairman.
- Records notes and conclusions, to be published on the wiki page / PRs or issues.

The secretary role rotates among attendees.

Coq Call Specific Organization
------------------------------

- The chairman, if not already designated, can be chosen at the beginning of the call along with a different person to act as secretary.
- Topics should be set by Monday 4pm to let the chair announce the call on Tuesday morning on Zulip and give notice to people around the world. 
  People that will participate may use an emoji reaction to the announcement to indicate their presence.
- If no topic is set by Monday 4pm, the call is cancelled.
- One can simply call for a roundtable session to get an update on everyone’s current work / roadmaps.
