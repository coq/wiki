The recommended installation method is through the Coq platform. Other installation methods are also documented below.

## Coq platform

The Coq platform is a distribution of the Coq proof assistant together
with a selection of Coq libraries. It provides a set of scripts to
compile and install opam, Coq, external Coq libraries and Coq plugins on macOS,
Windows and many Linux distributions in a reliable way with consistent
results.

### Binary package

Beginners are advised to install the binary using the [Snap package](https://snapcraft.io/coq-prover).

### Installation from sources

Experienced users can rely on the Coq platform interactive scripts to install the Coq platform from sources using opam.
This will create an opam switch with Coq and a standard set of packages, which can then be modified, e.g., by installing additional packages.

See the Linux specific instructions for the Coq platform scripts here: https://github.com/coq/platform/blob/2021.02/README_Linux.md#installation-by-compiling-from-sources-using-opam

Once the Coq platform is set up, you can install additional Coq packages by running `opam install coq-packagename`.

### User interfaces

You will need a user interface to run Coq. The Coq platform bundles CoqIDE, which is a Coq-specific editor. Alternatively, you can install editor-support packages for VsCode, Emacs or Vim. See https://coq.inria.fr/user-interfaces.html for details.

## Manual installation of Coq through opam

Complete instructions are available at https://coq.inria.fr/opam-using.html. They can be useful if you need to manage several versions of Coq and use specific versions of the OCaml compiler. Otherwise, we advise installing Coq through the Coq platform interactive scripts (see above).

## Installation from package repositories

[![latest packaged version(s)](https://repology.org/badge/latest-versions/coq.svg)](https://repology.org/metapackage/coq/versions)

![Arch package](https://repology.org/badge/version-for-repo/arch/coq.svg) \
![Debian unstable package](https://repology.org/badge/version-for-repo/debian_unstable/coq.svg)
![Debian 13 package](https://repology.org/badge/version-for-repo/debian_13/coq.svg)
![Debian 12 package](https://repology.org/badge/version-for-repo/debian_12/coq.svg)
![Debian 11 package](https://repology.org/badge/version-for-repo/debian_11/coq.svg)
![Debian 10 package](https://repology.org/badge/version-for-repo/debian_10/coq.svg) \
![Fedora Rawhide package](https://repology.org/badge/version-for-repo/fedora_rawhide/coq.svg)
![Fedora 39 package](https://repology.org/badge/version-for-repo/fedora_39/coq.svg)
![Fedora 38 package](https://repology.org/badge/version-for-repo/fedora_38/coq.svg)
![Fedora 37 package](https://repology.org/badge/version-for-repo/fedora_37/coq.svg)
![Fedora 36 package](https://repology.org/badge/version-for-repo/fedora_36/coq.svg) \
![Gentoo package](https://repology.org/badge/version-for-repo/gentoo/coq.svg) \
![Manjaro testing package](https://repology.org/badge/version-for-repo/manjaro_testing/coq.svg)
![Manjaro stable package](https://repology.org/badge/version-for-repo/manjaro_stable/coq.svg) \
![nixpkgs unstable package](https://repology.org/badge/version-for-repo/nix_unstable/coq.svg)
![NixOS 22.05 package](https://repology.org/badge/version-for-repo/nix_stable_22_05/coq.svg)
![NixOS 21.11 package](https://repology.org/badge/version-for-repo/nix_stable_21_11/coq.svg) \
![openSUSE Tumbleweed package](https://repology.org/badge/version-for-repo/opensuse_tumbleweed/coq.svg)
![openSUSE Leap 15.5 package](https://repology.org/badge/version-for-repo/opensuse_leap_15_5/coq.svg) \
![Ubuntu 24.04 package](https://repology.org/badge/version-for-repo/ubuntu_24_04/coq.svg)
![Ubuntu 22.04 package](https://repology.org/badge/version-for-repo/ubuntu_22_04/coq.svg)
![Ubuntu 20.04 package](https://repology.org/badge/version-for-repo/ubuntu_20_04/coq.svg)

Depending on your distribution, your package manager may include an up-to-date Coq package, or a very outdated one.
On any Linux distribution, you can rely on [[Nix]] to get the latest stable version (or even the development version) and some additional packages.

To check that the installation is successful, run the command `coqc -v`.

To check that CoqIDE is correctly installed, run the command:

```
coqide &
```
You may want to change the default [key bindings for CoqIDE](Configuration%20of%20CoqIDE).

To install Proof-General, refer to the [official Proof-General website](https://proofgeneral.github.io/#quick-installation-instructions).