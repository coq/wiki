Some of the tools listed here are part of bigger projects that support other proof assistants/theorem provers. Another list of Coq-related tools can be found at <https://coq.inria.fr/related-tools>.

Interface for Editing Proofs
============================

-   [CoqIDE](CoqIde)

    > The graphical user interface distributed with Coq.

-   [ProofGeneral](https://proofgeneral.github.io/)

    > ProofGeneral is a generic interface for proof assistants, based on the customizable text editor Emacs.

-   [Coqoon](http://coqoon.github.io)

    > Eclipse plugin for Coq development (based on Wenzel's asynchronous PIDE framework).

-   [ProofWeb](http://prover.cs.ru.nl)

    > An online web interface for Coq (and other proof assistants), with a focus on teaching.

-   [vscoq](https://github.com/siegebell/vscoq)

    > A Visual Studio based interface developed in 2016.

-   [jscoq](https://github.com/ejgallego/jscoq)

    > A port of Coq to the browser; runs standalone using `js_of_ocaml`.

-   [CoqTail](https://github.com/whonore/Coqtail)

    > CoqTail is a vim plugin aiming to bring the interactivity of CoqIDE into your favorite editor.

Language Servers
----------------
- [coq-lsp](https://github.com/ejgallego/coq-lsp/)
     > coq-lsp is a [Language Server](https://microsoft.github.io/language-server-protocol/) and [Visual Studio Code](https://code.visualstudio.com/) extension for Coq. Experimental support for [Vim](https://github.com/ejgallego/coq-lsp/#vim) and [Neovim](https://github.com/ejgallego/coq-lsp/#neovim) is also available in their own projects.
Key [features](https://github.com/ejgallego/coq-lsp/#Features) of coq-lsp are continuous and incremental document checking, advanced error recovery, markdown support, positional goals and information panel, performance data, and more.
-   [coq-serapi](https://github.com/ejgallego/coq-serapi)

    > A language-server based in the STM protocol with full serialization capabilities [proofs, documents, terms].

Discontinued interfaces
-----------------------

-   [Coq PIDE](http://coqpide.bitbucket.org)

    > Jedit (proof of concept) plugin for Coq development by Carst Tankink (also based on asynchronous PIDE framework).

-   [ProverEditor](http://provereditor.gforge.inria.fr)

    > An experimental Eclipse plugin with support for Coq.

-   [tmEgg](http://www.cs.ru.nl/~lionelm/tmEgg/)

    > Coq plugin for TeXmacs

-   [Papuq](http://www.mimuw.edu.pl/~chrzaszc/Papuq/)

    > Papuq is patched version of [CoqIde](CoqIde) with teaching oriented features.

-   [GeoProof](http://home.gna.org/geoproof/)

    > GeoProof was a dynamic geometry software, which can communicate with CoqIDE to build the formula corresponding to a geometry figure interactively.

-   [PCoq](http://www-sop.inria.fr/lemme/pcoq/) (for versions of Coq in old syntax, version 7.4 of 2003 and before)

    > A graphical user interface for Coq. The environment provides ways to edit structurally formulas and commands, new notations can easily be added. It allows proof by pointing.

-   [TmCoq](http://tmcoq.audebaud.org/)

    > TmCoq integrates Coq within TeXmacs.

Interface for Browsing Proofs
=============================

-   [Helm](http://helm.cs.unibo.it/) is a browsable and searchable (using the `Whelp` tool) repository of formal mathematics (includes the Coq User Contributions).

Presenting Proofs
=================

-   `coqdoc` exports vernacular file to TeX or HTML. It is part of the Coq distribution and documented in the [Reference Manual](https://coq.inria.fr/refman/practical-tools/utilities.html#documenting-coq-files-with-coqdoc).

Tactics packages
================

-   [Micromega](https://coq.inria.fr/refman/addendum/micromega.html): solves linear (Fourier-Motzkin) and non linear (Sum-of-Square's algorithm) systems of polynomial inequations; also provides a (partial) replacement for the Coq's `omega` tactic.
-   [Ssreflect](https://coq.inria.fr/refman/proof-engine/ssreflect-proof-language.html) facilitates proof by small scale reflection, "a style of proof that ... provide\[s\] effective automation for many small, clerical proof steps. This is often accomplished by restating ("reflecting") problems in a more concrete form ... For example, in the Ssreflect library arithmetic comparison is not an abstract predicate, but a function computing a boolean. ([source](http://pauillac.inria.fr/pipermail/coq-club/2008/003486.html))"
-   [AAC Tactics](https://github.com/coq-community/aac-tactics) provides tactics that facilitates the rewriting of universal equations,modulo associative and possibly commutative operators, and modulo neutral elements (units).

Packaging extracted code
========================

-   [Z\_interface](ZInterfacePackage) An approach for deriving directly standalone programs from extracted code.

Automation
==========
- [CoqHammer/sauto](https://coqhammer.github.io/)
- [Tactician](https://github.com/coq-tactician)
- [Proverbot 9001](https://proverbot9001.ucsd.edu/)
- [Trakt](https://ecrancemerce.github.io/trakt/#/)
- [Proofster](https://proofster.cs.umass.edu/)
- [SMTCoq](https://smtcoq.github.io/)
- [Sniper](https://github.com/smtcoq/sniper/)
- [MirrorSolve](https://github.com/jsarracino/mirrorsolve)
- [Hierarchy Builder](https://github.com/math-comp/hierarchy-builder)
- [Elpi](https://github.com/LPCIC/coq-elpi/blob/master/coq-builtin.elpi)
- [Equations](https://github.com/mattam82/Coq-Equations)
- [itauto](https://gitlab.inria.fr/fbesson/itauto)
- [MirrorCore/RTac](https://github.com/gmalecha/mirror-core)

The following automation projects are academic research projects which may or may not be maintained.
- [ASTactic/CoqGym](https://github.com/princeton-vl/CoqGym) ([paper](https://arxiv.org/abs/1905.09381))
- [TacTok](https://github.com/LASER-UMASS/TacTok) ( [paper](https://people.cs.umass.edu/~brun/pubs/pubs/First20oopsla.pdf) )
- [PassPort](https://github.com/LASER-UMASS/TacTok) ( [paper](https://arxiv.org/pdf/2204.10370.pdf) )


Utilities
=========

-   `coqwc` is a stand-alone command line tool to print line statistics about Coq files (how many lines are spec, proof, comments). `coqwc` comes with the standard Coq distribution.

- [coq-tools](https://github.com/JasonGross/coq-tools) is a collection of scripts for manipulating Coq developments:

  + <a name=bug-minimizer />[`find-bug.py`](https://github.com/JasonGross/coq-tools/blob/master/find-bug.py) is a "bug minimizer" that automatically minimizes a `.v` file producing an error, allowing automatic creation of small stand-alone test cases for Coq bugs;

  + [`absolutize-imports.py`](https://github.com/JasonGross/coq-tools/blob/master/absolutize-imports.py) processes `.v` files, replacing things like `Require Import Program.` with `Require Import Coq.Program.Program.`, making `Require`s robust against shadowing of file names;

  + [`inline-imports.py`](https://github.com/JasonGross/coq-tools/blob/master/inline-imports.py) inlines the `Require`d dependencies of a `.v` file, allowing the automatic creation of stand-alone files from developments;

  + [`minimize-requires.py`](https://github.com/JasonGross/coq-tools/blob/master/minimize-requires.py) removes unused `Require` and `Require Import` statements, and, optionally, unused `Require Export` statements;

  + [`move-requires.py`](https://github.com/JasonGross/coq-tools/blob/master/move-requires.py) splits all `Require Import` and `Require Export` statements into separate `Require` and `Import`/`Export` statements, and moves all `Require` statements to the top of a `.v` file;

  + [`move-vernaculars.py`](https://github.com/JasonGross/coq-tools/blob/master/move-vernaculars.py) lifts many vernacular commands and inner lemmas out of `Proof. ... Qed.` blocks;

  + [`proof-using-helper.py`](https://github.com/JasonGross/coq-tools/blob/master/proof-using-helper.py) processes the output of running `coqc` on a file with `Suggest Proof Using` set, to modify the file with the given `Proof using` suggestions;