_This page gathers known issues about the CI infrastructure._

You can use the label [`kind: ci-failure`](https://github.com/coq/coq/issues?q=is%3Aopen+is%3Aissue+label%3A%22kind%3A+ci-failure%22) to filter issues about unwanted / unexpected failures in the CI.

You can use the label [`kind: infrastructure`](https://github.com/coq/coq/issues?q=is%3Aopen+is%3Aissue+label%3A%22kind%3A+infrastructure%22) for more generic issues about the infrastructure, CI included.

# Migration to Gitlab CI

Some notes about documentation on the backend setup:

- "we had https://docs.gitlab.com/ee/administration/settings/continuous_integration.html#keep-the-latest-artifacts-for-all-jobs-in-the-latest-successful-pipelines checked (I'm unchecking now, it was already unchecked on .com)"
  note that we push many branches on gitlab, c.f. discussion at https://coq.zulipchat.com/#narrow/stream/237656-Coq-devs-.26-plugin-devs/topic/Migration.20to.20Inria.20GitLab.20for.20CI.20.28instead.20of.20GitLab.2Ecom.29/near/392347126
- Inria custom runners docs https://gitlab.inria.fr/inria-ci/custom-runner/

# Previous entries

The previous entries on this page have been archived, please look in the history if you need to access them.