# Notes on the release process of Coq 8.11

## Time-based release cycle

Tentative schedule:

  - November 1st, 2019: branch `v8.11`
  - December 1st, 2019: version 8.11+β1
  - January 1st, 2020: release

## Items to include in the release

jfehrle:

https://github.com/coq/coq/pull/10489 - Fix output for "Printing Dependent Evars Line" - waiting for @ejgallego to do a final review since early August, only minor changes since then and low-risk.  I suppose @herbelin and @ppedrot won't feel a need to review this.

https://github.com/coq/coq/pull/10494 - Show diffs in "Show Proof." - I've been working on a smarter way of doing diffs for this case.  If I get that working by say, October 14, I'd like to get that in 10.11.  And if not, I'll submit the current code, which is a useful but lesser step.

https://github.com/coq/coq/pull/10020 - Show diff highlights in the Show, Show <num> and Show <id> commands.  Needs some routine changes.  I'd like to get this in, too.

