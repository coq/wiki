Please see the specific wiki page for each Coq release:

- [Release Schedule for Rocq 9.0](Release-Schedule-for-Rocq-9.0)
- [Release Schedule for Coq 8.20](Release-Schedule-for-Coq-8.20)
- [Release Schedule for Coq 8.19](Release-Schedule-for-Coq-8.19)
- [Release Schedule for Coq 8.18](Release-Schedule-for-Coq-8.18)
- [Release Schedule for Coq 8.17](Release-Schedule-for-Coq-8.17)
- [Release Schedule for Coq 8.16](Release-Schedule-for-Coq-8.16)
- [Release Schedule for Coq 8.15](Release-Schedule-for-Coq-8.15)
- [Release Schedule for Coq 8.14](Release-Schedule-for-Coq-8.14)
- [Release Schedule for Coq 8.13](Release-Schedule-for-Coq-8.13)
- [Release Plan for Coq 8.12](Release-Plan-for-Coq-8.12)
- [Release Plan for Coq 8.11](Release-Plan-for-Coq-8.11)
