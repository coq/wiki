[Coq](https://coq.inria.fr/) is a proof assistant based on the calculus of constructions. It is used to formalize proofs in a variety of fields, including mathematics and programming languages. Cocorico is the Coq wiki.

:warning: **Note**: this wiki has been converted in October 2017 from
an earlier wiki based on MoinMoin. [Some help on this Wiki](HelpOnGithubWiki).
[More about this migration](WikiMigration).

Coq installation
================

You need to install Coq and an appropriate IDE. Currently supported IDE's are CoqIDE (recommended for beginners), Proof General (major mode for Emacs users), Visual Studio with the [VSCoq](https://marketplace.visualstudio.com/items?itemName=maximedenes.vscoq) extension, and [Coqtail](https://github.com/whonore/Coqtail) for Vim.

For most users, it is easiest to install the [Coq platform](https://github.com/coq/platform) which comes with a broad selection of stable packages tested for interoperability. 
- Installation of the Coq system:
  - [Installation of Coq on Linux](Installation%20of%20Coq%20on%20Linux)
  - [Installation of Coq on Windows](Installation%20of%20Coq%20on%20Windows)
  - [Installation of Coq on Mac](Installation%20of%20Coq%20on%20Mac)
- [Configuration of CoqIDE](Configuration%20of%20CoqIDE)
- [Configuration of Proof General](Configuration%20of%20Proof%20General) (to be completed)
- [Configuration of coq-lsp for VScode](Configuration-of-Coq-LSP)

Coq official resources
======================

- [Reference Manual](https://coq.inria.fr/distrib/current/refman/)
- [Documentation of the Standard Library](https://coq.inria.fr/library/)
- [Discourse Forum](https://coq.discourse.group) (see also [[Discourse]] wiki page)
- [Zulip Chat](https://coq.zulipchat.com/)
- [Coq-Club mailing list](https://sympa.inria.fr/sympa/info/coq-club)
- [Bug tracker](https://github.com/coq/coq/issues)
- [FAQ](The-Coq-FAQ)
- [Twitter account](https://twitter.com/CoqLang)

Coq informal resources
======================

- IRC channel: `irc://irc.libera.chat/#coq`
- [Sub-reddit](https://www.reddit.com/r/Coq/)
- [Stack Overflow tag](https://stackoverflow.com/questions/tagged/coq)

Coq books and tutorials
=======================

- [Software Foundations](https://softwarefoundations.cis.upenn.edu)
  - Volume 1: Logical Foundations (by Benjamin C. Pierce et al.)
  - Volume 2: Programming Language Foundations (by Benjamin C. Pierce et al.)
  - Volume 3: Verified Functional Algorithms (by Andrew W. Appel)
  - Volume 4: QuickChick (by Leonidas Lampropoulos and Benjamin C. Pierce)
- [Certified Programming with Dependent Types](http://adam.chlipala.net/cpdt/), by Adam Chlipala
- [Coq'Art](http://www.labri.fr/perso/casteran/CoqArt/index.html), by Yves Bertot and Pierre Castéran
- [Mathematical Components book](https://math-comp.github.io/mcb/), by Assia Mahboubi and Enrico Tassi
- [Formal Reasoning About Programs](http://adam.chlipala.net/frap/), by Adam Chlipala
- [Programs and Proofs](https://ilyasergey.net/pnp/), by Ilya Sergey
- [Computer Arithmetic and Formal Proofs](http://iste.co.uk/book.php?id=1238), by Sylvie Boldo and Guillaume Melquiond
- [Program Logics for Certified Compilers](https://www.cambridge.org/us/academic/subjects/computer-science/programming-languages-and-applied-logic/program-logics-certified-compilers), by Andrew W. Appel et al.
- [Teaching Coq and teaching with Coq](CoqInTheClassroom)
- [Hydras & Co.](https://github.com/coq-community/hydra-battles), accompanying textbook [here](https://coq-community.org/hydra-battles/doc/hydras.pdf).

Coq development
===============

- [The Contributing Guide](https://github.com/coq/coq/blob/master/CONTRIBUTING.md#contributing-to-coq)
- [Coq weekly Calls](Coq-Calls)
- [Coq Working Groups](Coq-Working-Groups)
- [The next Coq Working Group](Next-Coq-Working-Group)
- [Ideas for the Google Summer of Code 2016](GoogleSummerOfCode)
- [Wishes for Coq](Wishes) (and a specific page of [wishes for unification](UnificationProblems))
- [Wishes for Graphical User Interfaces](GUIWishes) (and a specific page for [CoqIDE wishes](CoqIDEWishes))
- [Basic architecture of Coq User Interfaces](Basic-architecture-of-Coq-User-Interfaces)
- [A development tutorial](DevelopmentTutorial)
- [Development frequently asked questions](Development-questions)
- [List of maintainers](https://github.com/coq/coq/blob/master/.github/CODEOWNERS)
- [Archive of discussions](DevelopmentArchive)
- [Setting up your development environment](DevelSetup)
- [State of the continuous integration infrastructure](State-of-the-continuous-integration-infrastructure)

Coq community
=============

Coq Users and Developers Workshops

- [2023 Coq Users and Developers Workshop](Coq-Users-and-Developers-Workshop-2023)
- [Previous Implementors Workshops](CoqImplementorsWorkshop)

Pointers to existing projects involving Coq.

- [Formal programming language research in Coq](List%20of%20Coq%20PL%20Projects)
- [Formal mathematics in Coq](List%20of%20Coq%20Math%20Projects)
- [Published works](Published-Works)

Coq libraries
=============

A non-exhaustive list of Coq libraries that are being used by other people than the developers.
- [Coq Platform](https://github.com/coq/platform) - A Coq installation for Windows, Linux, and MacOS together with a large family of libraries chosen for stability and tested for interoperability. For Coq 8.16 the precise list is [here](https://github.com/coq/platform/blob/main/doc/PackageTable~8.16~2022.09.csv)
- [Coq Community](https://github.com/coq-community/) - a repository containing numerous and diverse Coq packages collectively maintained by the Coq community.
- [Mathematical Components](http://math-comp.github.io/math-comp/): formalization of mathematical theories, focusing in particular on group theory.
- [Flocq](http://flocq.gforge.inria.fr): formalization of floating-point computations.
- [TLC](http://www.chargueraud.org/softs/tlc/): a non-constructive alternative to Coq's standard library.
- [ExtLib](https://github.com/coq-ext-lib/coq-ext-lib): a collection of theories and plugins that may be useful in other Coq developments.
- [CoLoR](http://color.inria.fr): a library on rewriting theory, lambda-calculus and termination, with sub-libraries on common data structures extending the Coq standard library (especially on vectors)
- [Coq-std++](https://gitlab.mpi-sws.org/iris/stdpp): an extended "Standard Library" for Coq
- [Alternative “Standard” Libraries](https://github.com/coq/stdlib2/wiki/Other-%22standard%22-libraries): list and descriptions of alternative standard libraries

(to be completed)

Coq plugins and tools
=====================

- [Plugins and tools for Coq](Tools)

(section to be updated)

Coq's logic
===========

- [Axioms that you might consider adding to Coq](CoqAndAxioms)
- [Papers about Coq's theoretical foundations](TheoryBehindCoq)

Coq Wiki
========

- [Some help on this Wiki](HelpOnGithubWiki).
- [More content](OtherContents)
- [List of recent changes](https://github.com/coq/coq/wiki/_history) (compare two revisions by selecting them then clicking on "Compare Revisions")
