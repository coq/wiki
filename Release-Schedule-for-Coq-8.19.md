RM for 8.19: Gaëtan Gilbert (co-RM: Matthieu Sozeau)

Release process issue: https://github.com/coq/coq/issues/18087

## Tentative schedule

- Branch: ~2023-11-27~ 2023-12-04
- 8.19+rc1: ~2023-12-11~ 2023-12-18
- 8.19.0: ~2024-01-16~ delayed for https://github.com/coq/coq/issues/18503

  2024-01-24
- 8.19.1: 2024-03-04
- 8.19.2: 2024-06-10
- 8.19.3: not planned