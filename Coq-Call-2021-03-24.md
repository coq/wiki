- March 24th 2021, 4pm-5pm Paris Time
- https://rdv2.rendez-vous.renater.fr/coq-call

## Topics

- PRS about typeclasses, and what where should we go from there. (Matthieu)
- ~asserting scopes on syndef arguments https://github.com/coq/coq/pull/13965 (Enrico)~

## Notes
    
- PRS about typeclasses.
  - Look into using the same uniform error messaging code
  - Long discussion about the design of Hint Extern If tac Then ... and possibly more
    expressive variants:
    We came to settle on keeping the `Hint Extern If` which allows to express one "global" cut on brothers 
    in the proof search tree + a reference to "self"/continue for solving subgoals with the right 
    options/depth/db etc. One can then use `once` for internal cuts if needed.