General tactics
===============

-   [TacticExts](TacticExts) small tactics that are widely useful

Domain specific tactics
=======================

-   [RingTactics](RingTactics) tactics for reasoning about ring structures
-   [InTac](InTac) tactic to prove the inclusion of list

Generic tactics
===============

-   [GenericTactics](GenericTactics) a few tactics letting the user define its domain specific tactics

Hypothesis iteration in Ltac and Ltac2
======================================

-   [generic iterators over hypotheses in a goal in Ltac and Ltac2](https://gist.github.com/jonleivent/62e1925b8702db9a0677ea02d4cf6cd3)