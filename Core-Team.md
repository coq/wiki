The core team is responsible for taking decisions regarding the system and its future, 
plan and realize releases, and manage the whole development process.

The current members can be seen [on the website](https://coq.inria.fr/coq-team.html).

The core team has a [voting process](https://github.com/coq/coq/wiki/Core-Team-Voting-Process).

Adding a new member
-------------------

The core team can decide to include new members, by invitation. When a new member accepts
the invitation, they will be added to the website page, the GitHub and GitLab core-team 
groups (as an owner) and the coq-core channel on Zulip.

Removing a member
-----------------

If a core team member is inactive for 6 months, the core team can decide to remove them from the team.
A core team member can also voluntarily step down at any point.

