### Goals

The goal of this working group is to coordinate the improvement of Coq
aspects related to user-interfaces. The WG is more focused on how Coq
can better support UI than on the development of UIs themselves which
is a bit orthogonal.

### Meetings

It seems that a bi-monthly meeting could work, but still to be
discussed, we will soon add a poll.

- Kick-off meeting (virtual): Friday October 14th, 10am-1pm / 2pm-5pm (if needed).

### Zulip Stream for Discussion

- https://coq.zulipchat.com/#narrow/stream/237661-User-interfaces.20devs.20.26.20users

### Participants [in alphabetical order]

- Emilio J. Gallego Arias
- Matthieu Sozeau

### Agenda

- 10am - 10.30am: Maxime and Enrico summarise their current work (following up from last WG)
- 10.30am - 12.30 pm: Emilio and Ali summarise their current work (with updates from last WG) 
- 12.30pm - 1pm: Discussion on how the two initiatives can work together and organization of development
  + Frequency and format of the WG
  + Freezing work on XML protocol / STM?
  + WG membership requirements

### Related projects

- Link Coq UI list
- coq-lsp: https://github.com/ejgallego/coq-lsp
- jsCoq: https://github.com/jscoq/jscoq

### Video

The meeting record is available on [YouTube](https://youtu.be/SsfvmFjUIrI)

