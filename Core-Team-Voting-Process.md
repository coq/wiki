These rules were approved by the Coq Core Team on January 10th 2023.

To resolve certain conflicting situations, the core team can proceed
to vote on a binary decision, as an ultimate recourse in case consensus
cannot be reached. The result binds the core team only and it is its 
responsibility to enforce the decision.

- Core team members each have a vote (including coordinator).
- Abstention is ok. Delegation of a vote is ok.
- Majority of >= 2/3 of casted votes (in Q)
- Votes take place online, anonymously, within a 72hr voting window.
- The vote must be announced with precise options 2 weeks in advance by
  the coordinator.
- Decision to take a vote is taken by the coordinator, taking
  input from the core team members.
- In case a majority of >= 2/3 cannot be reached, the coordinator can 
  decide to launch a second vote with the same announcement conditions and
  same rules except it is to be decided by > 50% majority. The coordinator's
  vote is decisive in case of a tie in this second round.