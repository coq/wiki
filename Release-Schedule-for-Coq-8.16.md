RM for 8.16: Pierre-Marie Pédrot (co-RM: Gaëtan Gilbert)

Release process issue: https://github.com/coq/coq/issues/16085

## Schedule

- Mid May 2022: branch
- July 2022: 8.16.0 tagging