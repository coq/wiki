- November 3rd 2021, 4pm-5pm Paris Time
- https://rdv2.rendez-vous.renater.fr/coq-call

# Topics

- https://github.com/coq/coq/pull/14857 move most ml code to `core/` (Gaëtan)

- Coq bug minimizer survey (Théo and Jason)
- Feedback on naming questions from the Coq survey (Théo)
- https://twitter.com/natfriedman/status/1453394825269571591 Github merge trains

## Notes

- https://github.com/coq/coq/pull/14857

  The goal of moving (most kernels) to core/ is to have the folder structure reflect
  the split of packages (although it's not perfect yet, e.g. plugins/checker are not moved yet)
  and to reduce the "clutter" at the toplevel directory. PMP opposes to use core instead of the
  historical convention of src, as core/ does not reflect the coq-core package entirely.
  A clearer commitment to a target configuration where core reflects the coq-core package  
  might help converge on this topic?

- Naming questions in the Coq survey: we agreed to not tie the name change to a software change
  and leave these two questions independent. The versioning questions will be up to the core team.
