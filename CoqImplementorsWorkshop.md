List of Implementors Workshops:
-------------------------------

- 2024, Nantes Université (Nantes) [Coq Users and Developers Workshop 2024](Coq-Users-and-Developers-Workshop-2024)
- 2023, Sophia-Antipolis (Nice) [Coq Users and Developers Workshop 2023](Coq-Users-and-Developers-Workshop-2023)
- 2022, On-line [Coq Hackathon and Working Group, Winter 2022](CoqWG-2022-02)
- 2020, On-line [Coq Users and Developers Workshop 2020](Coq-Users-and-Developers-Workshop-2020)
- 2019, Sophia-Antipolis (Nice) [Coq Users and Developers Workshop 2019](Coq-Users-and-Developers-Workshop-2019)
- 2018, Nice [Coq Implementors Workshop 2018](Coq-Implementors-Workshop-2018).
- 2017, Port-aux-rocs, Le Croisic (Nantes) [Coq Implementors Workshop 2017](CoqIW2017).
- 2016, Sophia-Antipolis (Nice) [Coq Implementors Workshop 2016](CoqIW2016).
- 2015, Sophia-Antipolis (Nice) [First Coq Coding Sprint](CoqCS1).
