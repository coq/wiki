:warning: This page has been superseded by https://rocq-prover.org/academic-users/institutions. To contribute, add or update an entry under https://github.com/coq/rocq-prover.org/tree/main/data/academic_institutions.

The objective of this page is to list all the universities where Coq is taught in courses or Coq is used in courses. We need your help to complete it. Please add new links to courses you are aware of. Universities are listed by country in alphabetical order.

## Austria

- TU Wien:
  - [192.098 Introduction to the Coq proof assistant](https://tiss.tuwien.ac.at/course/courseDetails.xhtml?dswid=2882&dsrid=538&courseNr=192098&semester=2020S&locale=en)

## Brazil
- Universidade de Brasilia:
  - [117366 Lógica Computacional 1](https://matriculaweb.unb.br/graduacao/disciplina.aspx?cod=117366)

## Canada

- University of Ottawa:
  - [CSI 5137 A (COMP 5900 N) Software Foundations](https://www.site.uottawa.ca/~afelty/csi5137/)

- University of Waterloo:
  - [CS 798: Software Foundations](https://cs.uwaterloo.ca/~plragde/798/)
## China

- Shanghai Jiao Tong University:
  - [Functional Programming in Coq](https://basics.sjtu.edu.cn/~yuxin/teaching/Coq/Coq2020.html)
- Nanjing University of Aeronautics and Astronautics:
  - Formalized Engineering Mathematics by Gang Chen

## Croatia

- University of Zagreb:
  - [Formalno verificirani algoritmi](https://www.pmf.unizg.hr/.cms/predmet_info?_v1=YYclQoMYVwCwcekegd9ohBCL1_NLgPTbp2fIhn5n__fn_SvMROFJjUKRkZHWXt6hSXefpRx_7ynu7VJGJ5zmd3hEah0nVvmwavvle5MAZDoMT7GfBgYhunyIU4zwiIwGrrUB3OV_IZ6Cv6HDd4Hyccog_ifXymS6TsBpuoRlNh96Opbd&_v1flags=Y0FlCgknfVykvQ3LLCPqI30aCAQR_GaSdLeSSPBc_i-Q9skKDR2uyK4zAPtK5t3CKvwsSjcgrJAR_pDXimKW3dXpivNZTwGk5DFor9t-JewOgz9zYvhm5Bm0i2OGul27BzH6D_OYn-QR_1O297oSsPLqwZy4khbyRrfYYQglgPqRq_1d&_lid=30775&_rand=0)

## Denmark

- Aarhus Universtity:
  - [Formal Software Verification](https://cs.au.dk/~spitters/fsv.html)
- ITU:
  - [Program Verification](https://learnit.itu.dk/local/coursebase/view.php?ciid=945)

## France

- École Polytechnique :
  - [INF551 - Computational Logic: Artificial Intelligence in Mathematical Reasoning](http://www.enseignement.polytechnique.fr/informatique/INF551/)
- É. N. S. de Lyon :
  - [Théorie de la programmation](https://perso.ens-lyon.fr/daniel.hirschkoff/ThPr/) (L3)
- Ens Paris-Saclay :
  - [Projet logique](https://lmf.cnrs.fr/AmelieLedein/ProjetLogique)
- Université de Bordeaux :
  - [Logique et preuve](https://www.labri.fr/perso/duchon/Enseignements/L-et-P/) (L3)
- Université Grenoble Alpes :
  - [Coq](http://www-verimag.imag.fr/~monin/Enseignement/Coq/)
- Université de Paris :
  - [Preuves assistées par ordinateur](https://github.com/herbelin/cours-preuves-ordinateur/)
  - [Assistants de preuve](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-7-2)
  - [Separation Logic Course](https://madiot.fr/sepcourse/)
  - [Functional programming and formal proofs in Coq](https://master.math.univ-paris-diderot.fr/en/modules/m2lmfi-preuveform/)
- Université de Strasbourg :
  - Preuves assistées par ordinateur (Master 1)
  - Constructions et preuves en géométrie (Master 2) 

## Germany

- Friedrich-Alexander-University Erlangen-Nuremberg:
  - [Praktische Semantik von Programmiersprachen](https://www8.cs.fau.de/teaching/ss20/semprog/)
- Saarland University:
  - [Introduction to Computational Logic](https://cms.sic.saarland/icl_21/)
  - [Advanced Coq Programming](https://courses.ps.uni-saarland.de/acp_20/)
- Technische Hochschule Nürnberg - Georg Simon Ohm:
  - [Ausgewählte Themen der Korrektheit und Semantik in Programmiersprachen](https://www.th-nuernberg.de/fakultaeten/in/studium/masterstudiengang-informatik/)
- University of Freiburg:
  - [Lab Course: The Coq Proof Assistant](https://proglang.informatik.uni-freiburg.de/teaching/coq/2015ss/)

## India

- Indian Institute of Technology Madras:
  - [CS6225 - Programs and Proofs](http://www.cse.iitm.ac.in/course_details.php?arg=MTQ5)
- Chennai Mathematical Institute
  - Interactive Theorem Proving, SP Suresh and MK Srivas

## Israel

- Ben Gurion University of the Negev:
  - [Logical Foundations using the Coq Proof Assistant](https://www.cs.bgu.ac.il/~lf202/Main)
- Tel Aviv University:
  - [Software Foundations in Coq](https://www30.tau.ac.il/yedion/syllabus.asp?course=0368313101&year=2018)
  - [Programming Language Foundations](https://www.ims.tau.ac.il/Tal/Syllabus/Syllabus_L.aspx?lang=EN&course=0368324101&year=2021&req=f5ffedef62e07a12a0a8515f6eaa3b139e611873a8baa9ea915bc37d80bf6bb3&caller=)

## Italy

- Università degli Studi di Udine
  - [Semantica dei Linguaggi di Programmazione](https://uniud.esse3.cineca.it/Guide/PaginaADErogata.do?ad_er_id=2021*N0*N0*P1*72710*13418&ANNO_ACCADEMICO=2021&mostra_percorsi=S)

## Hungary

- Eötvös Loránd University
  - Formális szemantika (Formal semantics)

## Mexico

- National Autonomous University of Mexico (UNAM), Faculty of Science
  - [Lógica computacional (Computational Logic)](https://sites.google.com/ciencias.unam.mx/logica2022-1/inicio)
  - [Métodos Formales (Formal Methods)](https://sites.google.com/ciencias.unam.mx/mformales/mformales-22-2)

## Poland

- University of Wrocław:
  - [Formalizacja języków programowania w systemie Coq (Formalization of programming languages in Coq)](https://zapisy.ii.uni.wroc.pl/offer/2499-formalizacja-jezykow-programowania-w-systemie-coq/)
  - [Obliczenia i wnioskowanie w systemie Coq (Computation and reasoning in Coq)](https://zapisy.ii.uni.wroc.pl/offer/obliczenia-i-wnioskowanie-w-systemie-coq_139/)
  - [Seminar: Program certification in Coq (discontinued)](https://zapisy.ii.uni.wroc.pl/offer/880_certyfikowane-oprogramowanie-w-systemie-coq/)

## Portugal

- Universidade da Beira Interior:
  - [Computação Fiável](http://www.di.ubi.pt/~desousa/CF/comfia.html)
  - [Programação Certificada](http://www.di.ubi.pt/~desousa/PC/pc.html)
- Universidade do Minho:
  - [Lógica da Programação](http://www.math.uminho.pt/Default.aspx?tabid=7&pageid=520&lang=pt-PT#LPG)
- Universidade de Lisboa:
  - [Programming Languages](https://fenix.tecnico.ulisboa.pt/disciplinas/LPro2646/2020-2021/2-semestre)

## The Netherlands

- Eindhoven University of Technology:
  - [Proving with Computer Assistance 2IMF15](http://www.cs.ru.nl/H.Geuvers/onderwijs/provingwithCA/)
- Radboud University
  - [Type Theory and Coq, IMC010](http://cs.ru.nl/~freek/courses/tt-2021/)
  - Logic and Applications

## Sweden

- Chalmers University of Technology:
  - [Coq @ Chalmers](https://github.com/vlopezj/coq-course)

## Switzerland

- EPFL
  - [Interactive Theorem Proving](https://edu.epfl.ch/coursebook/en/interactive-theorem-proving-cs-CS-628)

## United States of America

- Caltech:
  - [CS 128: Interactive Theorem Proving](https://www.cms.caltech.edu/academics/courses/cs-128)
  - [CS 118: Automata-Theoretic Software Analysis](https://spinroot.com/spin/Doc/course/index.html)
- Carnegie Mellon University:
  - [15–414/614 Bug Catching: Automated Program Verification](https://www.cs.cmu.edu/~emc/15414-s14/)
  - [15-811 Verifying Complex Systems](http://www.cs.cmu.edu/~15811/)
- Cornell University:
  - [CS 4160 Formal Verification](https://www.cs.cornell.edu/courses/cs4160/2020sp/)
- MIT:
  - [Fundamentals of Program Analysis](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-820-fundamentals-of-program-analysis-fall-2015/lecture-notes/)
  - [Formal Reasoning about Programs](https://frap.csail.mit.edu/)
- Northern Arizona University:
  - [CS 499 Mechanized Reasoning about Programs](https://frederic.loulergue.eu/teaching/MRAP/)
- Pomona College:
  - [CSCI 054 Discrete Mathematics and Functional Programming](https://cs.pomona.edu/~michael/courses/csci054s20/)
- Princeton University:
  - [Computer Science 510 Programming Languages](https://www.cs.princeton.edu/courses/archive/spring15/cos510/)
- Rice University:
  - [COMP 403 Reasoning about Software](https://mamouras.web.rice.edu/courses/Spring-2021-COMP-403-503/)
- UNC Greensboro:
  - [CSC 495/693: Software Foundations](https://home.uncg.edu/cmp/faculty/srtate/495.f16/index.php)
- University of Chicago
  - [CMSC 22400/32400: Programming Proofs.](http://collegecatalog.uchicago.edu/search/?P=CMSC%2022400)
- University of Illinois at Chicago:
  - [CS 494 SF: Software Foundations](https://www.cs.uic.edu/~mansky/teaching/cs494sf/sp19/syllabus.html)
- University of Kansas
  - [EECS 755: Software Requirements Modeling and Analysis](https://perry.alexander.name/eecs755/)
  - [EECS 843: Programming Language Foundations II](https://perry.alexander.name/eecs843)
- University of Maryland:
  - [CMSC 631: Program Analysis and Understanding](https://www.cs.umd.edu/class/fall2021/cmsc631/index.html)
- University of Massachusetts:
  - [CS 420: Introduction to the Theory of Computation](https://cogumbreiro.github.io/teaching/cs420/s20/)
- University of Pennsylvania:
  - [CIS 500: Software Foundations](https://www.seas.upenn.edu/~cis500/current/index.html)
- University of Rochester 
  - Machine-checked Proofs using Coq by [Chen Ding](https://www.cs.rochester.edu/~cding/)