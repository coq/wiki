Gaëtan Gilbert is the release manager for 8.15.

The following issue can be used to track the release process: https://github.com/coq/coq/issues/15014

## Schedule

- Branching date / feature freeze: 2021-11-15
- 8.15+rc1 tag: 2021-12-06 (after [issues tagged blocker](https://github.com/coq/coq/issues?q=is%3Aopen+is%3Aissue+milestone%3A8.15%2Brc1+label%3A%22priority%3A+blocker%22) fixed)
- 8.15.0 release: 2022-01-03 if no major issues reported in the RC
  delayed waiting for release summary

  actual date: 2022-01-13
- 8.15.1: 2022-03-22
- 8.15.2: 2022-05-31